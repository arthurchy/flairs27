The 27th International FLAIRS Conference
----------------------------------------
May 21 - 23, 2014

**Yang Chen, Milenko Petrovic, Micah H. Clark**  
yang@cise.ufl.edu, {mpetrovic,mclark}@ihmc.us

**Abstract**  
Semantic memory techniques are currently facing significant scalability
challenges due to the growth of problem complexity and data size. In this
paper, we present SemMemDB, an in-database module for semantic memories. This
module defines a novel relational model for semantic memories and an efficient
SQL-based spreading activation algorithm. We provide an intuitive, easy-to-use
API for the users to run retrieval tasks. SemMemDB is the first in-database
query engine for semantic memories. The benefits of our in-database approach
includes: 1) Databases have mature query engine and optimizer that generates
efficient execution plans; 2) Databases provide massive storage that supports
large amount of data; 3) SQL is one of the industry standards used widely in
various systems and frameworks.  We provide extensive evaluation on DBPedia, a
web-scale knowledge base constructed from Wikipedia articles. Experiment
results show our system runs over 500 times faster than previous works.
